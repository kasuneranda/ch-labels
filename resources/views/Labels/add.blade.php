@extends('Admin.layout')
@section('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet"/>
@endsection
@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    @if (isset($model))
                        @include('Admin.partials.breadcumbs',['header' => $model->label,'params' => $model])
                    @else
                        @include('Admin.partials.breadcumbs',['header'=>__('general.Create Label')])
                    @endif
                </div>
            </div>
        </div>
    </div>
    @include('Admin.partials.form-alert')

    <div class="content-body">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        @if (isset($model))
                            <h4 class="card-title" id="basic-layout-form-center">{{__('labels.Update Label')}}</h4>
                        @else
                            <h4 class="card-title" id="basic-layout-form-center">{{__('labels.Create new label')}}</h4>
                        @endif
                    </div>
                    <div class="card-content show">
                        <div class="card-body">
                            @if(isset($model))
                            <form action="{{ route('admin.labels.update',['label'=>$model->id]) }}" method="POST"
                                  class="form-horizontal" enctype="multipart/form-data">
                                @else
                                    <form action="{{route('admin.labels.store')}}" method="POST"
                                          class="form-horizontal" enctype="multipart/form-data">
                                @endif
                                {{ csrf_field() }}

                                @if (isset($model))

                                    <input type="hidden" name="_method" value="PATCH">

                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input type="hidden" name="id" id="id" class="form-control" readonly="readonly" value="{{$model->id}}">
                                        </div>
                                    </div>
                                    @else
                                            <input type="hidden" name="id" id="id" class="form-control" readonly="readonly" value="">
                                @endif
                                @include('Admin.partials.multi-language-select-locale', [
                                    'model' => isset($model) ? $model : null,
                                    'setRowCol' => false,
                                    'labelClass' => 'col-sm-3 control-label',
                                    'formElemInDiv' => true,
                                    'formElemDivClass' => 'col-sm-4'
                                ])
                                <div class="form-group">
                                    <label for="first_name" class="col-sm-3 control-label">{{__('labels.Name')}}</label>
                                    <div class="col-sm-4">
                                        <input required type="text" name="label" id="label" class="form-control" value="{{isset($model) ? $model->label : (old('label'))?? '' }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="example-color-input" class="col-sm-3 control-label">{{__('labels.Color')}}</label>
                                    <div class="col-sm-2">
                                        <input required class="form-control height-100 width-100" type="color" value="{{isset($model) ? $model->color : (old('label'))?? '' }}" name="color" id="example-color-input">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="example-color-input" class="col-sm-3 control-label">{{__('labels.Target')}}</label>
                                    <div class="col-sm-4">
                                        <select required  name="target" id="target" class="form-control" data-placeholder="select a user">
                                            <option value="0" disabled selected>{{__('labels.select a plugin')}}</option>
                                            @foreach($targets as $target=>$relatedModel)
                                                  <option value="{{$relatedModel}}" {{ isset($model) ? ($model->model == $relatedModel) ? 'selected' : (old('target'))?? '' :''}}>{{$target}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                        <div class="form-actions left">
                                            <button type="reset" class="btn btn-warning mr-1">
                                                <i class="ft-x"></i> {{__('trainings.general.reset')}}
                                            </button>

                                            <a href="{{ URL::previous() }}">
                                                <button type="button" href="" class="btn btn-warning mr-1">
                                                    <i class="ft-arrow-left"></i> {{__('trainings.general.goBack')}}
                                                </button></a>
                                            <button type="button" class="btn btn-primary" id="saveForm">
                                                <i class="fa fa-check-square-o"></i> {{__('trainings.general.update')}}
                                            </button>
                                            {{--<button type="submit" class="btn btn-primary" id="saveAndGoBack">
                                                <i class="fa fa-check-square-o"></i> {{__('trainings.general.updateAndBack')}}
                                            </button>--}}
                                        </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
    <script src="{{ asset("js/scripts/labels_scripts.js")}}"></script>
    <script>
        $(document).ready(function () {
            $('select[name="form_locale"]').on('change', function (e) {
                let slctLocale = $(this);
                let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                let id = slctLocale.closest('form').find('#id').val();
                let form_locale = slctLocale.val();
                @if(isset($model))
                    let url = '{{ route('admin.labels.edit', ['label' => $model->id]) }}';
                @else
                    let url = '{{ url('admin/labels') }}/' + id + '/edit';
                @endif

                if (id) {
                    $.ajax({
                        data: {_token: CSRF_TOKEN, id: id, form_locale: form_locale},
                        method: 'GET',
                        url: url,
                    }).done(function (object) {
                        try {
                            if (object.data.label) {
                                slctLocale.closest('form').find('input[name="label"]').val(object.data.label);
                            }

                            if (object.data.color) {
                                slctLocale.closest('form').find('input[name="color"]').val(object.data.color);
                            }
                        } catch (e) {
                            console.log(e);
                        }
                    }).fail(function (error) {
                        console.log(error);
                        let messages = error.responseJSON.msg;

                        for (let message in messages) {
                            toastr.error(messages[message], 'Error', {timeOut: 5000});
                        }
                    });
                }
            });
        });
    </script>
@endsection
