<?php

namespace Creativehandles\ChLabels\Http\Controllers\PluginsControllers;

use App\Http\Controllers\Controller;
use App\Plugin;
use App\Traits\MultilanguageTrait;
use Creativehandles\ChLabels\Plugins\Labels\Labels;
use Creativehandles\ChLabels\Plugins\Labels\Models\Label;
use App\Traits\UploadTrait;
use Creativehandles\ChLabels\Plugins\Labels\Repositories\LabelRepository;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Validator;

class LabelsController extends Controller
{
    use MultilanguageTrait, UploadTrait;

    protected $views = 'Admin.Labels.';
    protected $logprefix = 'LabelsController';

    /**
     * @var Labels
     */
    private $labels;

    /**
     * @var LabelRepository
     */
    private $labelRepository;

    public function __construct(Request $request, LabelRepository $labelRepository, Labels $labels)
    {
        $this->setLocaleInRequestHeader($request);
        $this->labelRepository = $labelRepository;
        $this->labels = $labels;
    }

    public function index(Request $request)
    {
        return view($this->views.'index', []);
    }

    public function create(Request $request)
    {
        //get all available plugins
        $plugins = Plugin::get();
        $targets = [];

        foreach ($plugins as $plugin) {
            $targets[$plugin->plugin] = config('labelModelMapping.'.$plugin->plugin, null);
        }
        //map them with the models related to tables
        return view($this->views.'add', [
            'targets'=>array_filter($targets),
        ]);
    }

    public function edit(Request $request, $id)
    {
        $model = Label::find($id);

        if ($request->ajax()) {
            if (isset($model)) {
                $model->setDefaultLocale($request->get('form_locale', app()->getLocale()));
                return response()->json(['data' => $model], 200);
            } else {
                return response()->json(['msg' => [__('labels.404')]], 404);
            }
        }

        //get all available plugins
        $plugins = Plugin::get();
        $targets = [];

        foreach ($plugins as $plugin) {
            $targets[$plugin->plugin] = config('labelModelMapping.'.$plugin->plugin, null);
        }

        return view($this->views.'add', [
            'model'=>$model,
            'targets'=>array_filter($targets),
        ]);
    }

    public function show(Request $request, $id)
    {

    }

    public function grid(Request $request)
    {
        $searchColumns = ['labels.model', 'label_translations.label', 'label_translations.color'];
        $searchValue = request()->input('search')['value'];
        $orderColumns = ['1' => 'label_translations.label', '3' => 'labels.model'];
        $where = isset($searchValue) && $searchValue !== '' ? ['label_translations.locale', '=', app()->getLocale()] : null;
        $data = $this->labelRepository->getDataforDataTables($searchColumns, $orderColumns, '1', [], $where);

        $data['data'] = array_map(function ($item) {
            return [
                $item['id'],
                $item['label'],
                $item['color'],
                $item['model'],
            ];
        }, $data['data']);

        echo json_encode($data);
    }

    public function update(Request $request)
    {
        $rules = [
            'label' => 'required',
            'color' => 'required',
            'target' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            Log::error($this->logprefix.'('.__FUNCTION__.'): Label data incomplete ', $request->all());

            if ($request->ajax()) {
                return response()->json(['msg' => $validator->errors()->toArray()], 400);
            } else {
                return redirect()->back()->withInput()->withErrors($validator);
            }
        }

        //format and save label
        $label = [
            'model' => $request->target,
            'locale' => $request->form_locale,
            'label' => $request->label,
            'color' => $request->color,
            'author' => \Auth::id(),
        ];

        $condition = [
            'model' => $request->target,
            'locale' => $request->form_locale,
            'label' => $request->label,
        ];

        try {
            if ($request->input('id', false)) {
                $storedLabel = $this->labels->createOrUpdateLabel($label, ['id'=>$request->id]);
            } else {
                $storedLabel = $this->labels->createOrUpdateLabel($label, $condition);
            }
        } catch (Exception $e) {
            if ($request->ajax()) {
                return response()->json(['msg' => 'Error Occured', 'developer_msg' => $e->getMessage()], 500);
            } else {
                return redirect()->back()->withInput()->with('error', 'Error Occured')->withInput();
            }
        }

        if ($request->ajax()) {
            return response()->json(['msg' => 'Label Saved', 'data' => $storedLabel], 200);
        } else {
            return redirect()->route('admin.labels')->with('success', 'Label added successfully!');
        }
    }

    public function store(Request $request)
    {
        return $this->update($request);
    }

    public function destroy(Request $request, $id)
    {
        $instructor = Label::findOrFail($id);
        $instructor->delete();

        return response()->json(['msg' => 'Label Deleted'], 200);
    }
}
