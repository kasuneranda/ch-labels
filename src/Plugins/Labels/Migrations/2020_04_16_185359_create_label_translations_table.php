<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLabelTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // create the translations table
        if (! Schema::hasTable('label_translations')) {
            Schema::create('label_translations', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('label_id');
                $table->string('locale', 10)->index();
                $table->string('label', 191);
                $table->string('color', 191);
                $table->unsignedInteger('author');
                $table->timestamps();

                $table->unique(['label_id', 'locale']);
                $table->foreign('label_id')->references('id')->on('labels')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('label_translations');
    }
}
